Rails.application.routes.draw do
  get 'dices/index', to: 'dices#index'
  get 'dices/room/:roomid', to: 'dices#room'
  get 'dices/roll', to: 'dices#roll'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
