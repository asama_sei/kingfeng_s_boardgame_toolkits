require "test_helper"

class DicesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dices_index_url
    assert_response :success
  end

  test "should get room" do
    get dices_room_url
    assert_response :success
  end

  test "should get roll" do
    get dices_roll_url
    assert_response :success
  end
end
